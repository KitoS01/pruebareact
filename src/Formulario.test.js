// Formulario.test.js
import React from 'react';
import { shallow } from 'enzyme';
import Formulario from './Formulario';



describe('Formulario', () => {
  it('debería llamar a la función handleSubmit cuando se envía el formulario', () => {
    // Creamos una función simulada para handleSubmit
    const handleSubmitMock = jest.fn();
    const wrapper = shallow(<Formulario handleSubmit={handleSubmitMock} />);

    // Simulamos el cambio en los campos de entrada
    wrapper.find('input[type="text"]').simulate('change', { target: { value: 'John Doe' } });
    wrapper.find('input[type="email"]').simulate('change', { target: { value: 'john@example.com' } });

    // Simulamos el envío del formulario
    wrapper.find('form').simulate('submit', { preventDefault: () => {} });

    // Verificamos si la función simulada handleSubmit fue llamada con los valores correctos
    expect(handleSubmitMock).toHaveBeenCalledWith('John Doe', 'john@example.com');
  });
});
