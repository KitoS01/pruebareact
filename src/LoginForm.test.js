// LoginForm.test.js
import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils'; // Importa act desde react-dom/test-utils
import LoginForm from './LoginForm';

const credentialsAreValid = true; // Define si las credenciales son válidas o no

if (credentialsAreValid) {
  test('Iniciar sesión con credenciales válidas', () => {
    act(() => { 
      render(<LoginForm />);
    });

    act(() => {
      fireEvent.change(screen.getByPlaceholderText('Nombre de usuario'), { target: { value: 'user' } });
      fireEvent.change(screen.getByPlaceholderText('Contraseña'), { target: { value: 'password' } });
    });

    act(() => {
      fireEvent.click(screen.getByText('Iniciar sesión'));
    });

    expect(screen.queryByText('Nombre de usuario o contraseña incorrectos.')).not.toBeInTheDocument();
  });
} else {
  test('Iniciar sesión con credenciales inválidas', () => {
    act(() => { 
      render(<LoginForm />);
    });

    act(() => {
      fireEvent.change(screen.getByPlaceholderText('Nombre de usuario'), { target: { value: 'usuario_invalido' } });
      fireEvent.change(screen.getByPlaceholderText('Contraseña'), { target: { value: 'contraseña_invalida' } });
    });

    act(() => {
      fireEvent.click(screen.getByText('Iniciar sesión'));
    });

    expect(screen.queryByText('Bienvenido, usuario_invalido!')).not.toBeInTheDocument();
  });
}
