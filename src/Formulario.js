// Formulario.js
import React, { useState } from 'react';

const Formulario = ({ handleSubmit }) => {
  const [nombre, setNombre] = useState('');
  const [email, setEmail] = useState('');

  const handleNombreChange = (event) => {
    setNombre(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    handleSubmit(nombre, email);
  };

  return (
    <form onSubmit={handleFormSubmit}>
      <input type="text" value={nombre} onChange={handleNombreChange} />
      <input type="email" value={email} onChange={handleEmailChange} />
      <button type="submit">Enviar</button>
    </form>
  );
};

export default Formulario;
