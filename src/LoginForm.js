// LoginForm.js
import React, { useState } from 'react';

const LoginForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [loginError, setLoginError] = useState(false);

  const handleLogin = () => {
    if (username === 'user' && password === 'password') {
      setLoggedIn(true);
      setLoginError(false);
    } else {
      setLoginError(true);
    }
  };

  return (
    <div>
      {!loggedIn ? (
        <form>
          <input type="text" placeholder="Nombre de usuario" value={username} onChange={e => setUsername(e.target.value)} />
          <input type="password" placeholder="Contraseña" value={password} onChange={e => setPassword(e.target.value)} />
          <button type="button" onClick={handleLogin}>Iniciar sesión</button>
          {loginError && <p>Nombre de usuario o contraseña incorrectos.</p>}
        </form>
      ) : (
        <p>Bienvenido, {username}!</p>
      )}
    </div>
  );
};

export default LoginForm;