// jest.setup.js
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// Ignorar advertencias de deprecación
console.warn = jest.fn((message) => {
  if (message.includes('DeprecationWarning')) return;
  console.warn(message);
});
